import os
import sys
import threading
from concurrent.futures import ThreadPoolExecutor, Future
from typing import Any, Dict, List, Union, Type, Tuple, Mapping

import orjson
import zmq
from zmq import Context

from kiara.utils import is_develop, is_debug
from queen.config import ContextSettings, CONTEXT_SETTINGS
from queen.defaults import ERROR_INDICATOR, OK_INDICATOR, PING_INDICATOR
from queen.exceptions import NoSuchStateException, StateException
from queen.models.states import State, STATE_DETAILS_TYPE, StateDetails, STATE_REGISTRY, \
    STATE_CONFIG_TYPE
from queen.utils.terminal import send_msg


def default_stdout_print(msg) -> None:
    """Default output format."""

    send_msg(f"[green]stdout[/green]: {msg}")


def default_stderr_print(msg) -> None:
    """Default error output format."""

    send_msg(f"[red]stderr[/red]: {msg}")


class ExecutorContext(object):

    def __init__(self, context_settings: ContextSettings, max_workers: int=0):

        self._context_settings: ContextSettings = context_settings
        if not max_workers:
            max_workers = min(32, (os.cpu_count() or 1) + 4)

        self._max_workers =  max_workers

    def send_event(self, topic: str, **data):
        print("----------------------")
        dbg(topic)
        if data:
            dbg(data)
        print("----------------------")

    def send_debug_event(self, topic: str, **data):

        if is_develop() or is_debug():
            print("----------------------")
            print(topic)
            if data:
                dbg(data)
            print("----------------------")

    def listen_loop(self) -> None:

        currently_running: Dict[str, (State, Future)] = {}
        resolved: Dict[str, bytes] = {}
        waiting_sockets: Dict[str, List[bytes]] = {}

        context = zmq.Context.instance()

        resolve_request = context.socket(zmq.ROUTER)
        resolve_request.bind(self._context_settings.resolve_listen_addr)

        finished_resolve = context.socket(zmq.REP)
        finished_resolve.bind(self._context_settings.finished_resolve_listen_addr)

        poller = zmq.Poller()
        poller.register(resolve_request, zmq.POLLIN)
        poller.register(finished_resolve, zmq.POLLIN)

        executor = ThreadPoolExecutor(max_workers=self._max_workers)

        try:
            while True:

                try:
                    socks = dict(poller.poll())
                except KeyboardInterrupt:
                    break

                if finished_resolve in socks:
                    message = finished_resolve.recv_multipart()
                    finished_resolve.send(OK_INDICATOR)

                    try:
                        self.send_debug_event("request.resolve_finished.unwrap", message=message)
                        state_id, state_details_data = self.unwrap_finished_result(message)
                        self.send_debug_event("request.resolve_finished.unwrapped", state_id=state_id, details=state_details_data)
                        resolved[state_id] = state_details_data
                    except Exception as e:
                        self.send_debug_event("request.resolve_finished.unwrapped.error", exception=e)
                        raise e

                    if state_id not in waiting_sockets.keys():
                        self.send_debug_event("request.resolve_finished.no_listener", state_id=state_id, msg="This is probably a bug.")
                    else:
                        self.send_event("request_resolved", state_id=state_id)
                        self.send_debug_event("request_resolve_finished.notify_listeners", state_id=state_id)
                        for addr in waiting_sockets.get(state_id, []):
                            self.send_debug_event("request.resolve_finished.notify_listener", state_id=state_id, listener=addr)
                            msg = [addr, b'', state_details_data]
                            resolve_request.send_multipart(msg)

                if resolve_request in socks:

                    message = resolve_request.recv_multipart()
                    self.send_debug_event("request.resolve.received", message=message)
                    addr = message.pop(0)
                    message.pop(0)
                    if message[0] == PING_INDICATOR:
                        self.send_debug_event("request.resolve.ping", addr=addr)
                        resolve_request.send_multipart([addr, b'', OK_INDICATOR])
                    else:
                        self.send_event("request.resolve.received", message=message)
                        try:
                            state = self.unwrap_request(message)
                            if state.state_id in resolved.keys():
                                self.send_event("request.resolve.respond.cached", state_id=state.state_id)
                                result_msg = [addr, b'', resolved[state.state_id]]
                                resolve_request.send_multipart(result_msg)
                            else:
                                waiting_sockets.setdefault(state.state_id, []).append(addr)

                                future = executor.submit(self.wrapped_resolve, state, context)
                                currently_running[state.state_id] = (state, future)
                                self.send_debug_event("request.resolve.respond.submitted", state_id=state.state_id, addr=addr)

                        except NoSuchStateException as e:
                            self.send_debug_event("request.resolve.received.invalid_message", message=message, exception=e)
                            resolve_request.send_multipart([addr, b'', ERROR_INDICATOR, str(e).encode()])
                        except Exception as e:
                            self.send_debug_event("request.resolve.received.error", message=message, exception=e)
                            resolve_request.send_multipart([addr, b'', ERROR_INDICATOR, str(e).encode()])

        except Exception as e:
            self.send_debug_event("request.error", exception=e)
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def unwrap_finished_result(self, message: Union[List[zmq.Frame], List[bytes]]) -> Tuple[str, bytes]:

        assert message[0] in [ERROR_INDICATOR, OK_INDICATOR]
        assert message[0] == OK_INDICATOR

        state_hash = message[1].decode()

        return state_hash, message[2]

    def unwrap_request(self, message: Union[List[zmq.Frame], List[bytes]]) -> State:

        state_cbor: bytes = message[0]

        if len(message) > 1:
            state_cid: bytes = message[1]
            state = State.from_cbor(data=state_cbor, cid=state_cid)
        else:
            state = State.from_cbor(data=state_cbor)

        state.set_context(self._context_settings.context_id)
        return state

    def wrapped_resolve(self, state: State, context: Context) -> None:

        print(f"RESOLVING: {state}")
        dbg(state.__dict__)
        dbg(state.config._state)
        self.send_event("resolve_state.started", state_id=state.state_id)

        import zmq
        client_socket = context.socket(zmq.REQ)
        client_socket.connect(self._context_settings.resolve_addr)

        # # TODO: we probably don't need this
        # client_socket.send(PING_INDICATOR)
        # ok = client_socket.recv(20)
        # assert ok == OK_INDICATOR

        details = None
        exception = None
        try:
            details = state.resolve()
            self.send_event("resolve_state.finished", state_id=state.state_id)
        except Exception as e:
            self.send_event("resolve_state.error", state_id=state.state_id, exception=e)
            exception = e

        context = zmq.Context.instance()
        client_socket = context.socket(zmq.REQ)
        client_socket.connect(self._context_settings.finished_resolve_addr)

        if exception is not None:
            msg = [b"ERROR", state.state_id.encode(), str(exception).encode()]
        else:
            msg = [b"OK", state.state_id.encode(), details.json().encode()]

        self.send_debug_event("request.resolve_finished.send", message=msg)
        client_socket.send_multipart(msg)
        repl = client_socket.recv_string()
        assert repl == "OK"

    def start(self, wait_for_socket: bool = True):

        self._socket_thread = threading.Thread(target=self.listen_loop, daemon=True)
        self._socket_thread.start()

        if wait_for_socket:
            context = zmq.Context.instance()
            client_socket = context.socket(zmq.REQ)
            client_socket.connect(self._context_settings.resolve_addr)
            client_socket.send(PING_INDICATOR)
            socket_up = client_socket.recv()
            assert socket_up == OK_INDICATOR

        return self._socket_thread


class StateContext(object):

    def __init__(self, context_config: Union[None, Mapping[str, Any]]=None, context_settings: Union[ContextSettings, None]=None):

        assert context_settings is None
        context_settings = CONTEXT_SETTINGS

        self._context_settings = context_settings

        if context_config is None:
            context_config = {}
        self._context_config = context_config

        self._states: Dict[str, State] = {}
        self._state_details: Dict[str, STATE_DETAILS_TYPE] = {}

        self._state_context = ExecutorContext(context_settings=self._context_settings)
        self._state_context.start(wait_for_socket=True)

        context = zmq.Context.instance()
        self._socket = context.socket(zmq.REQ)
        self._socket.connect(self._context_settings.resolve_addr)

    @property
    def context_id(self) -> str:
        return self._context_settings.context_id

    def get_context_config(self, key: str) -> Any:
        return self._context_config[key]

    def add_state(self, state: Union[State, Type[State], str], state_alias: Union[str, None]=None) -> str:
        """Add a state to this context.

        Args:
            state: the state
            state_alias: an alias for the state. If not provided, the state's id will be used.

        Returns:
            The alias under which this state was registered.
        """

        if state_alias is None:
            state_alias = state.state_id

        if state_alias in self._states.keys():
            raise Exception(f"State with alias {state_alias} already exists")

        if not isinstance(state, State):
            if isinstance(state, str):
                state_cls = STATE_REGISTRY.state_types.get(state, None)
                if state_cls is None:
                    raise NoSuchStateException(f"No state type with id {state} registered.")
            else:
                if not issubclass(state, State):
                    raise StateException(f"State type classs {state} is not a subclass of 'State'.")
                else:
                    state_cls = state
            try:
                state = state_cls()
            except Exception as e:
                raise StateException(f"State type {state} can't be initialized without configuration: {e}")
        else:
            # TODO: clone state obj?
            pass

        state.set_context(self.context_id)

        self._states[state_alias] = state


    def is_resolved(self, state_alias: str) -> bool:

        state = self._states.get(state_alias, None)
        if state is None:
            raise NoSuchStateException(f"No state registered with alias: '{state_alias}'.")

        details = self._state_details.get(state_alias, None)
        return details is not None

    def retrieve_state_details(self, state: State[STATE_CONFIG_TYPE, STATE_DETAILS_TYPE]) -> STATE_DETAILS_TYPE:

        self._socket.send_multipart([state.cbor, bytes(state.cid)])
        details_raw = self._socket.recv()
        details = orjson.loads(details_raw)
        result = state.Details(**details)
        return result

    def get_state_details(self, state: Union[str, State]) -> StateDetails:

        if isinstance(state, str):
            state_alias = state
        else:
            state_alias = None
            for alias, st in self._states.items():
                if st.state_id == st.state_id:
                    state_alias = alias
                    break

            if state_alias is None:
                raise NoSuchStateException(f"No state registered with id: '{state.state_id}'.")

        if self.is_resolved(state_alias):
            return self._state_details[state_alias]

        _state = self._states[state_alias]
        details = self.retrieve_state_details(_state)
        self._state_details[state_alias] = details
        return details

