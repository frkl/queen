from functools import cached_property

from pydantic import BaseSettings, Field

class ContextSettings(BaseSettings):

    context_id: str = Field(description="The id of the current context.", default="default")

    resolve_addr: str = Field(description="The zmq address of the resolve state endpoint.", default="tcp://127.0.0.1:5555")
    resolve_listen_addr: str = Field(description="The zmq address of the resolve state endpoint.", default="tcp://127.0.0.1:5555")

    finished_resolve_addr: str = Field(description="The zmq address of the finished resolve state endpoint.", default="tcp://127.0.0.1:5556")
    finished_resolve_listen_addr: str = Field(description="The zmq address of the finished resolve state endpoint.", default="tcp://127.0.0.1:5556")

    event_addr: str = Field(description="The zmq address of the event endpoint.", default="tcp://localhost:5557")
    event_listen_addr: str = Field(description="The zmq address of the event endpoint.", default="tcp://127.0.0.1:5557")

    class Config:
        env_prefix = 'queen_'  # defaults to no prefix, i.e. ""

    # @property
    # def state_registry(self) -> StateRegistry:
    #     return STATE_REGISTRY
    #
    # @cached_property
    # def state_contextx(self) -> StateContext:
    #     return StateContext(self)


CONTEXT_SETTINGS = ContextSettings()
