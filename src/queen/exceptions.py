class StateException(Exception):
    pass

class InvalidStateException(StateException):
    pass

class NoSuchStateException(StateException):
    pass

class StateResolveException(StateException):
    pass
