import os
from functools import cached_property, lru_cache
from typing import Union, Any
from typing.io import IO


def setup_dbg_helper() -> None:

    try:
        from rich import inspect
        from rich import print as rich_print

        import builtins
        _dbg = getattr(builtins, "DBG", None)

        if _dbg is not None:
            return

        setattr(builtins, "insp", inspect)

        def dbg(
            *objects: Any,
            sep: str = " ",
            end: str = "\n",
            file: Union[IO[str], None] = None,
            flush: bool = False,
        ):

            for obj in objects:
                try:
                    rich_print(obj, sep=sep, end=end, file=file, flush=flush)
                except Exception:
                    rich_print(
                        f"[green]{obj}[/green]", sep=sep, end=end, file=file, flush=flush
                    )

        setattr(builtins, "dbg", dbg)

        def DBG(
            *objects: Any,
            sep: str = " ",
            end: str = "\n",
            file: Union[IO[str], None] = None,
            flush: bool = False,
        ):

            objs = (
                ["[green]----------------------------------------------[/green]"]
                + list(objects)
                + ["[green]----------------------------------------------[/green]"]
            )
            dbg(*objs, sep=sep, end=end, file=file, flush=flush)

        setattr(builtins, "DBG", DBG)

    except ImportError:  # Graceful fallback if IceCream isn't installed.
        pass

@lru_cache(maxsize=1)
def is_debug() -> bool:

    debug = os.environ.get("DEBUG", "")
    if debug.lower() == "true":
        return True
    else:
        return False


@lru_cache(maxsize=1)
def is_develop() -> bool:

    develop = os.environ.get("DEVELOP", "")
    if not develop:
        develop = os.environ.get("DEV", "")

    if develop and develop.lower() != "false":
        return True

    return False
