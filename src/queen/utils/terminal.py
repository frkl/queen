import os
from typing import Union, Any, Mapping

from rich.console import Console
from rich.panel import Panel

_console = None

def get_console() -> Console:
    """Get a global Console instance.

    Returns:
        Console: A console instance.
    """
    global _console
    if _console is None or True:
        console_width = os.environ.get("CONSOLE_WIDTH", None)
        width = None

        if console_width:
            try:
                width = int(console_width)
            except Exception:
                pass

        _console = Console(width=width)

    return _console


def send_msg(
    msg: Union[Any, None] = None,
    target: Union[str, None] = None,
    in_panel: Union[str, None] = None,
    rich_config: Union[Mapping[str, Any], None] = None,
    empty_line_before: bool = False,
    **config: Any,
) -> None:

    if not target:
        target = "terminal"

    if target == "terminal":

        if msg is None:
            msg = ""
        console = get_console()

        if in_panel is not None:
            msg = Panel(msg, title_align="left", title=in_panel)

        if empty_line_before:
            console.print()
        if rich_config:
            console.print(msg, **rich_config)
        else:
            console.print(msg)
    else:
        raise NotImplementedError(f"Output target '{target}' not implemented.")
