from typing import Union, Iterable, Any, Mapping, Dict

import httpx


def get_all_pkg_data_from_pypi(
        pkg_name: str,
        version: Union[str, None, int, float] = None,
        extras: Iterable[str] = None,
) -> Dict[str, Any]:

    if version:
        url = f"https://pypi.org/pypi/{pkg_name}/{version}/json"
    else:
        url = f"https://pypi.org/pypi/{pkg_name}/json"

    result = httpx.get(url)

    if result.status_code >= 300:
        raise Exception(
            f"Could not retrieve information for package '{pkg_name}': {result.text}"
        )
    pkg_metadata = result.json()
    return pkg_metadata

def get_pkg_metadata_from_pypi(
        pkg_name: str,
        version: Union[str, None, int, float] = None,
        extras: Iterable[str] = None,
) -> Mapping[str, Any]:

    result = get_all_pkg_data_from_pypi(
        pkg_name=pkg_name, version=version, extras=extras
    )
    _result = result["info"]
    if version is None:
        _r: Mapping[str, Iterable[Dict[str, Any]]] = result["releases"]
    else:
        _r = {version: result.pop("urls")}

    _result["releases"] = _r
    return _result
