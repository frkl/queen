"""
The `queen` main module.
"""

from queen.utils import setup_dbg_helper

setup_dbg_helper()
