# -*- coding: utf-8 -*-

#  Copyright (c) 2022, Markus Binsteiner

"""A command-line interface for *queen*.
"""

import logging
import os
import rich_click as click
import structlog
import sys
from pathlib import Path
from rich.markdown import Markdown
from typing import Tuple, Union

from kiara.context.config import KiaraConfig
from kiara.defaults import (
    KIARA_CONFIG_FILE_NAME,
    KIARA_MAIN_CONFIG_FILE,
    SYMLINK_ISSUE_MSG,
)
from kiara.interfaces.python_api import KiaraAPI
from kiara.utils import is_debug
from kiara.utils.class_loading import find_all_cli_subcommands
from kiara.utils.cli import terminal_print
from queen.interfaces.cli.package import package
from queen.mgmt import StateContext

# from .service.commands import service

# click.rich_click.USE_MARKDOWN = True
click.rich_click.USE_RICH_MARKUP = True


if is_debug():
    structlog.configure(
        wrapper_class=structlog.make_filtering_bound_logger(logging.DEBUG),
    )
else:
    structlog.configure(
        wrapper_class=structlog.make_filtering_bound_logger(logging.WARNING),
    )

CLICK_CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])

@click.group(context_settings=CLICK_CONTEXT_SETTINGS)
@click.pass_context
def cli(ctx: click.Context) -> None:
    """*queen* cli"""

    ctx.obj = {}
    ctx.obj["state_ctx"] = StateContext()
    # sys.exit(0)

cli.add_command(package)
# cli.add_command(conda)

if __name__ == "__main__":
    cli()
