# # -*- coding: utf-8 -*-
# import os
# import sys
# from typing import Union
#
# import rich_click as click
# from kiara.utils.cli import terminal_print
# from kiara.utils.files import get_data_from_file
#
# from queen.mgmt.conda import CondaContext
# from queen.models import PkgSpec
#
#
# #  Copyright (c) 2022, Markus Binsteiner
# #
# #  Mozilla Public License, version 2.0 (see LICENSE or https://www.mozilla.org/en-US/MPL/2.0/)
#
#
# @click.group("conda")
# @click.pass_context
# def conda(ctx):
#     """Conda environment related sub-commands."""
#
#     pass
#
#
# @conda.command()
# @click.argument("pkg_spec", nargs=1, required=True)
# @click.option(
#     "--publish", "-p", is_flag=True, help="Whether to upload the built package."
# )
# @click.option(
#     "--user",
#     "-u",
#     help="If publishing is enabled, use this anaconda user instead of the one directly associated with the token.",
#     required=False,
# )
# @click.option(
#     "--token",
#     "-t",
#     help="If publishing is enabled, use this token to authenticate.",
#     required=False,
# )
# @click.pass_context
# def build_package_from_spec(
#     ctx,
#     pkg_spec: str,
#     publish: bool = False,
#     token: Union[str, None] = None,
#     user: Union[str, None] = None,
# ):
#     """Create a conda environment."""
#
#     if publish and not token:
#         if not os.environ.get("ANACONDA_PUSH_TOKEN"):
#             terminal_print()
#             terminal_print(
#                 "Package publishing enabled, but no token provided. Either use the '--token' cli option or populate the 'ANACONDA_PUSH_TOKEN' environment variable."
#             )
#             sys.exit(1)
#     conda_mgmt: CondaContext = ctx.obj["conda_context"]
#
#     recipe_data = get_data_from_file(pkg_spec)
#     pkg = PkgSpec(**recipe_data)
#
#     pkg_result = conda_mgmt.build_package(pkg)
#     if publish:
#         conda_mgmt.upload_package(pkg_result, token=token, user=user)
#
#
#
#
#
# @conda.command()
# @click.argument("pkg")
# @click.option("--version", "-v", help="The version of the package.", required=False)
# @click.option(
#     "--patch-data", "-p", help="A file to patch the auto-generated spec with."
# )
# @click.option(
#     "--publish", "-p", is_flag=True, help="Whether to upload the built package."
# )
# @click.option(
#     "--user",
#     "-u",
#     help="If publishing is enabled, use this anaconda user instead of the one directly associated with the token.",
#     required=False,
# )
# @click.option(
#     "--token",
#     "-t",
#     help="If publishing is enabled, use this token to authenticate.",
#     required=False,
# )
# @click.option(
#     "--force-version", help="Overwrite the Python package version number.", is_flag=True
# )
# @click.pass_context
# def build_package(
#     ctx,
#     pkg: str,
#     version: str,
#     publish: bool = False,
#     user: Union[str, None] = None,
#     token: Union[str, None] = None,
#     patch_data: Union[str, None] = None,
#     force_version: bool = False,
# ):
#     """Create a conda environment."""
#
#     if publish and not token:
#         if not os.environ.get("ANACONDA_PUSH_TOKEN"):
#             terminal_print()
#             terminal_print(
#                 "Package publishing enabled, but no token provided. Either use the '--token' cli option or populate the 'ANACONDA_PUSH_TOKEN' environment variable."
#             )
#             sys.exit(1)
#
#     conda_mgmt: CondaContext = ctx.obj["conda_context"]
#     _patch_data = None
#     if patch_data:
#         _patch_data = get_data_from_file(patch_data)
#
#     metadata = conda_mgmt.get_pkg_metadata(
#         pkg=pkg, version=version, force_version=force_version
#     )
#     pkg = conda_mgmt.create_pkg_spec(pkg_metadata=metadata, patch_data=_patch_data)
#
#     terminal_print()
#     terminal_print("Generated conda package spec:")
#     terminal_print()
#     terminal_print(pkg.create_conda_spec())
#     terminal_print()
#     terminal_print("Building package...")
#     pkg_result = conda_mgmt.build_package(pkg)
#     if publish:
#         conda_mgmt.upload_package(pkg_result, token=token, user=user)
#
#     dbg(pkg_result)
