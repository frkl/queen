# -*- coding: utf-8 -*-
import abc
import sys
import uuid
from functools import cached_property
from pathlib import Path

from typing import Any, Mapping, Union, Generic, TYPE_CHECKING, TypeVar, Dict, Type

import dag_cbor
import zmq
from boltons.strutils import slugify
from multiformats import CID, multihash
from orjson import orjson
from pydantic import BaseModel, PrivateAttr, Field

from queen.defaults import QUEEN_STATE_BASE_FOLDER, ERROR_INDICATOR
from queen.exceptions import InvalidStateException, NoSuchStateException
from queen.utils.hashing import calculate_cid
from queen.utils.json import orjson_dumps


if TYPE_CHECKING:
    from queen.config import ContextSettings

class StateConfig(BaseModel):

    class Config:
        extra = "forbid"
        frozen = True
        json_loads = orjson.loads
        json_dumps = orjson_dumps

    _context_id: str = PrivateAttr(None)

    _config_id_cache: str = PrivateAttr(default=None)
    _state_dir_cache: Path = PrivateAttr(default=None)

    _cbor_cache: bytes = PrivateAttr(default=None)
    _cid_cache: CID = PrivateAttr(default=None)

    _state: "State" = PrivateAttr(None)

    @property
    def config_id(self) -> str:
        if self._config_id_cache is not None:
            return self._config_id_cache

        self._config_id_cache = self._create_config_id()  # noqa
        return self._config_id_cache
    @property
    def state_dir(self) -> Path:

        if self._state_dir_cache is not None:
            return self._state_dir_cache

        if self._state is None:
            raise InvalidStateException("The state config is not attached to a state context.")

        self._state_dir_cache = Path(QUEEN_STATE_BASE_FOLDER) / self._state._context_id / self.config_id  # noqa
        return self._state_dir_cache

    def cbor(self) -> bytes:
        if self._cbor_cache is not None:
            return self._cbor_cache

        dag, cid = calculate_cid(data=self.dict())

        self._cid_cache = cid  # noqa
        self._cbor_cache = dag  # noqa

        return self._cbor_cache

    @property
    def cid(self) -> CID:
        if self._cid_cache is not None:
            return self._cid_cache

        dag, cid = calculate_cid(data=self.dict())

        self._cid_cache = cid  # noqa
        self._cid_cache = dag  # noqa

        return self._cid_cache

    def _create_config_id(self) -> str:
        return str(self.cid)

class OneOffConfig(StateConfig):
    def _create_config_id(self) -> str:
        return str(uuid.uuid4())


class StateDetails(BaseModel):

    class Config:
        extra = "forbid"
        frozen = True
        json_loads = orjson.loads
        json_dumps = orjson_dumps

    _state: "State" = PrivateAttr(None)

STATE_CONFIG_TYPE = TypeVar("STATE_CONFIG_TYPE", bound=StateConfig)
ONE_OFF_CONFIG_TYPE = TypeVar("ONE_OFF_CONFIG_TYPE", bound=OneOffConfig)
STATE_DETAILS_TYPE = TypeVar("STATE_DETAILS_TYPE", bound=StateDetails)


class StateRegistry(object):

    def __init__(self):
        self._all_states: Dict[str, Type[State]] = {}

    def register(self, state_cls: Type["State"]):

        if state_cls.state_type_id in self._all_states:
            raise InvalidStateException(f"Can't register state type '{state_cls.__name__}': type with that name already registered.")

        self._all_states[state_cls.state_type_id] = state_cls

    @property
    def state_types(self) -> Mapping[str, Type["State"]]:
        return self._all_states

STATE_REGISTRY = StateRegistry()

class State(Generic[STATE_CONFIG_TYPE, STATE_DETAILS_TYPE]):

    class Config(StateConfig):
        pass

    class Details(StateDetails):
        pass

    def __init_subclass__(cls, /, state_type_id: Union[str, None]=None, **kwargs):

        super().__init_subclass__(**kwargs)

        if not state_type_id:
            state_type_id = slugify(cls.__name__, delim="_")

        cls.state_type_id = state_type_id

        STATE_REGISTRY.register(cls)

    @classmethod
    def create_from_config(cls, **config) -> "State[STATE_CONFIG_TYPE, STATE_DETAILS_TYPE]":
        """Create this state instance, using a to-be-parsed configuration."""

        config = cls.Config(**config)
        return cls(state_config=config)


    @classmethod
    def from_cbor(cls, data: bytes, cid: Union[None, CID, bytes]=None) -> "State":

        if cid is not None:

            hash_codec = "sha2-256"
            encode: str = "base58btc"
            hash_func = multihash.get(hash_codec)
            digest = hash_func.digest(data)

            new_cid = CID(encode, 1, codec="dag-cbor", digest=digest)
            if isinstance(cid, CID):
                assert new_cid == cid
            else:
                assert bytes(new_cid) == cid

        state_data = dag_cbor.decode(data)
        state_type_id = state_data["state_type"]

        state_type_cls = STATE_REGISTRY.state_types.get(state_type_id, None)
        if state_type_cls is None:
            raise NoSuchStateException(f"No such state: {state_type_id}")

        state_config_data = state_data["state_config"]
        state = state_type_cls(state_config=state_config_data)
        return state

    def __init__(self, state_config: Union[None, STATE_CONFIG_TYPE, Mapping[str, Any]]=None, context_settings: Union[None, "ContextSettings"]=None):

        assert context_settings is None
        from queen.config import CONTEXT_SETTINGS
        self._context_settings = CONTEXT_SETTINGS

        if state_config is None:
            state_config = self.__class__.Config()
        elif isinstance(state_config, Mapping):
            state_config = self.__class__.Config(**state_config)

        if state_config._state is not None:
            raise Exception("Can't reuse config instance.")

        self._config: STATE_CONFIG_TYPE = state_config
        self._details: Union[None, STATE_DETAILS_TYPE] = None

        assert self._config._state is None
        self._config._state = self
        self._state_dependencies: Union[Mapping[str, State]] = None

        self._config_cbor_cache = None
        self._cid_cache = None
        self._context_id: Union[None, str] = None


    @property
    def is_idempotent(self) -> bool:
        return self._is_idempotent()

    def _is_idempotent(self) -> bool:
        return False

    @property
    def dependencies(self) -> Mapping[str, "State"]:

        if self._state_dependencies is not None:
            return self._state_dependencies

        deps = {}
        deps_raw = self._retrieve_dependencies()
        if deps_raw:
            for n, d in deps_raw.items():
                if isinstance(d, State):
                    deps[n] = d
                elif issubclass(d, State):
                    try:
                        deps[n] = d()
                    except Exception as e:
                        raise InvalidStateException(f"Can't create state dependency '{d.__name__}' without configuration: {e}")
                else:
                    raise InvalidStateException(f"Invalid state dependency type: {d}")

        self._state_dependencies = deps
        return self._state_dependencies



    def _retrieve_dependencies(self) -> Mapping[str, Union["State", Type["State"]]]:
        return {}

    @property
    def cid(self) -> CID:
        """A cid for this model."""

        if self._cid_cache is not None:
            return self._cid_cache

        obj = {
            "state_type": self.state_type_id,
            "state_config": self.config.dict()
        }
        dag, cid = calculate_cid(data=obj)

        self._cid_cache = cid
        self._config_cbor_cache = dag
        return self._cid_cache

    @property
    def cbor(self) -> bytes:
        """A cid for this model."""

        if self._config_cbor_cache is not None:
            return self._config_cbor_cache

        obj = {
            "state_type": self.state_type_id,
            "state_id": self.cid,
            "state_config": self.config.dict()
        }
        dag, cid = calculate_cid(data=obj)

        self._cid_cache = cid
        self._config_cbor_cache = dag
        return self._config_cbor_cache

    @property
    def state_dir(self) -> Path:
        """The directory for this state."""

        return self.config.state_dir

    def set_context(self, context_id: str):

        if self._context_id is not None:
            raise Exception("Can't set context id twice.")
        self._context_id = context_id

        if self.config._context_id is not None:
            raise Exception("Can't set context id twice on state config.")
        self.config._context_id = context_id

    @cached_property
    def state_id(self) -> str:
        return f"{self.state_type_id}_{self.cid}"

    def get_dependency_details(self, dependency_name: str) -> StateDetails:

        if dependency_name not in self.dependencies.keys():
            raise Exception(f"Dependency '{dependency_name}' not found.")

        return self._get_state_details(self.dependencies[dependency_name])

    def _get_state_details(self, state: "State[STATE_CONFIG_TYPE, STATE_DETAILS_TYPE]") -> STATE_DETAILS_TYPE:

        context = zmq.Context.instance()
        client_socket = context.socket(zmq.REQ)
        client_socket.connect(self._context_settings.resolve_addr)

        print(f"SENT DEPENDENCY REQUEST: {state.state_type_id}")
        client_socket.send_multipart([state.cbor, bytes(state.cid)])
        details_raw = client_socket.recv_multipart()
        if details_raw[0] == ERROR_INDICATOR:
            raise Exception("Error getting state details.")
        print("DETAILS_RAW")
        dbg(details_raw)
        details = orjson.loads(details_raw[0])
        print(f"RECEIVED: {details}")
        result = state.Details(**details)
        return result

    @property
    def config(self) -> STATE_CONFIG_TYPE:
        return self._config

    def check(self) -> Union[None, STATE_DETAILS_TYPE]:

        if self._details is not None:
            return self._details

        self._details = self._check()
        return self._details

    def resolve(self) -> STATE_DETAILS_TYPE:

        try:
            print(f"RESOLVING: {self.__class__.__name__}")

            details = self.check()
            if details is not None:
                return details

            state_details = self._resolve()
            if state_details is None:
                raise Exception(
                    f"No state details for: {self.__class__.__name__}. This is a bug."
                )

            if isinstance(state_details, Mapping):
                state_details = self.__class__.Details(**state_details)

            return state_details
        except Exception as e:
            import traceback
            traceback.print_exc()
            raise e

    def purge(self):

        self._details = None
        self._purge()

    @abc.abstractmethod
    def _check(self) -> Union[None, STATE_DETAILS_TYPE]:
        pass

    @abc.abstractmethod
    def _resolve(self) -> Union[Mapping[str, Any], STATE_DETAILS_TYPE]:
        pass

    @abc.abstractmethod
    def _purge(self):
        pass



class IdempotentState(State[STATE_CONFIG_TYPE, STATE_DETAILS_TYPE]):

    class Config(OneOffConfig):
        pass

    def _is_idempotent(self) -> bool:
        return True

class OneOff(State[ONE_OFF_CONFIG_TYPE, STATE_DETAILS_TYPE]):

    def _is_idempotent(self) -> bool:
        return False

    def _check(self) -> Union[None, STATE_DETAILS_TYPE]:
        return None
