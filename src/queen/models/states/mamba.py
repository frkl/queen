import io
import json
import os
import platform
import shutil
import subprocess
import tarfile
import tempfile
import urllib
from functools import cached_property
from pathlib import Path
from typing import Union, Mapping, Any, List, Type

from pydantic import Field

from queen.defaults import QUEEN_DEFAULT_MICROMAMBA_TARGET_PREFIX
from queen.exceptions import StateResolveException
from queen.models import PkgSpec, RunDetails
from queen.models.states import StateConfig, StateDetails, IdempotentState, OneOff, OneOffConfig, State
from queen.utils.terminal import send_msg


class MicromambaAvailable(IdempotentState, state_type_id="micromamba_available"):

    class Config(StateConfig):

        base_path: Union[str, None] = Field(description="The path where to put the micromamba binary.", default=None)
        version: Union[str, None] = Field(description="The version of micromamba.", default=None)

    class Details(StateDetails):

        micromamba_path: str = Field(description="The path to micromamba.")

    @cached_property
    def micromamba_path(self) -> Path:

        base_path = self.config.base_path
        version_str = self.config.version
        if not version_str:
            version_str = ""
        else:
            version_str = f"_{version_str}"

        micromamba_name = f"micromamba{version_str}"

        if base_path is None:
            base_path = os.path.join(self.state_dir, "micromamba")
        base_path = os.path.join(base_path, "bin", micromamba_name)

        return Path(base_path)

    def _check(self) -> Union[None, "MicromambaAvailable.Details"]:

        if not self.micromamba_path.is_file():
            return None

        # TODO: check if right version
        return self.__class__.Details.construct(micromamba_path=self.micromamba_path)

    def _purge(self):

        os.unlink(self.micromamba_path)

    def _resolve(self) -> "MicromambaAvailable.Details":

        current = self._check()
        if current is not None:
            return current

        this_arch = platform.machine().lower()
        this_os = platform.system().lower()

        ARCH_MAP = {
            "linux": {
                "x86_64": "linux-64",
                "amd64": "linux-64",
                "64bit": "linux-64",
                "aarch64": "linux-aarch64",
                "ppc64": "linux-ppc64le",
            },
            "darwin": {
                "arm64": "osx-arm64",
                "x86": "osx-64",
                "64bit": "osx-64",
                "amd64": "osx-64",
            },
            "windows": {},
        }

        token = ARCH_MAP.get(this_os, {}).get(this_arch, None)
        if token is None:
            raise Exception(
                f"No micromamba executable available for: {this_os} / {this_arch}."
            )

        url = f"https://micro.mamba.pm/api/micromamba/{token}/latest"

        # bin_path = root_path / "bin" / "micromamba"
        # bin_path.parent.mkdir(parents=True, exist_ok=True)
        send_msg("Downloading micromamba...")

        fh = io.BytesIO()
        with urllib.request.urlopen(url) as response:
            fh.write(response.read())

        fh.seek(0)
        tar_archive = tarfile.open(fileobj=fh, mode="r:bz2")
        temp_directory = tempfile.TemporaryDirectory()
        try:
            tar_archive.extract("bin/micromamba", temp_directory.name)
            self.micromamba_path.parent.mkdir(parents=True, exist_ok=True)
            shutil.move(os.path.join(temp_directory.name, "bin", "micromamba"), self.micromamba_path)
        finally:
            shutil.rmtree(temp_directory.name, ignore_errors=True)

        current = self._check()
        if current is not None:
            return current
        else:
            raise StateResolveException("Something went wrong.")

class MambaEnvironmentConfig(StateConfig):

    env_name: str = Field(description="The name of the environment.")
    channels: List[str] = Field(description="The channels to use for the environment.", default_factory=list)
    dependencies: List[str] = Field(description="The conda dependencies that should be present in the environment.", default_factory=list)
    mamba_prefix: Union[str, str] = Field(description="The base path for the environments.", default=None)
    python_version: Union[str, None] = Field(description="The version of python to use.", default=None)

class MambaEnvironmentDetails(StateDetails):

    env_name: str = Field(description="The name of the environment.")
    env_path: str = Field(description="The path to the environment.")
    python_path: str = Field(description="The path to the Python executable.")
    # python_version: str = Field(description="The version of Python that is used.")
    mamba_prefix: str = Field(description="The base path for the environments.")

class MambaEnvironment(IdempotentState[MambaEnvironmentConfig, MambaEnvironmentDetails]):

    Config = MambaEnvironmentConfig
    Details = MambaEnvironmentDetails

    def _retrieve_dependencies(self) -> Mapping[str, Union[State, Type[State]]]:

        deps = {
            "micromamba": MicromambaAvailable
        }
        return deps

    @cached_property
    def micromamba_prefix(self) -> Path:

        mamba_prefix = self.config.mamba_prefix
        if mamba_prefix is None:
            mamba_prefix = QUEEN_DEFAULT_MICROMAMBA_TARGET_PREFIX

        return Path(mamba_prefix)

    def _check(self) -> Union[None, Mapping[str, Any]]:

        env_name: str = self.config.env_name

        env_path = Path(os.path.join(self.micromamba_prefix.as_posix(), env_name))
        python_path = Path(os.path.join(env_path, "bin", "python"))

        if not env_path.is_dir() or not python_path.is_file():
            return None

        # TODO: check details of env

        return {
                "env_name": env_name,
                "env_path": env_path,
                "python_path": python_path,
                "mamba_prefix": self.micromamba_prefix.as_posix(),
            }

    def _purge(self):

        shutil.rmtree(self.micromamba_prefix)

    def _resolve(self) -> Mapping[str, Any]:

        check = self._check()
        if check is not None:
            return check

        micromamba_path = self.get_dependency_details("micromamba").micromamba_path

        env_name = self.config.env_name
        channels = self.config.channels
        dependencies = self.config.dependencies

        channels_str = "\n".join((f"  - {c}" for c in channels))
        dependencies_str = "\n".join((f"  - {d}" for d in dependencies))

        spec_content = f"""name: {env_name}
channels:
{channels_str}
dependencies:
{dependencies_str}
"""
        handle, filename = tempfile.mkstemp(text=True, suffix=".yaml")
        with os.fdopen(handle, "wt") as f:
            f.write(spec_content)

        send_msg(
            f"Creating conda environment '{env_name}'. This will take a while..."
        )

        args = [
            micromamba_path,
            "create",
            "--yes",
            "--json",
            "-p",
            os.path.join(self.micromamba_prefix.as_posix(), env_name),
            "-f",
            filename,
        ]
        result = subprocess.run(args, capture_output=True, text=True, shell=False)

        if filename != None:
            os.unlink(filename)

        if result.returncode != 0:
            print(f"Error creating environment '{env_name}':")
            print("stdout:")
            print(result.stdout)
            print("stderr:")
            print(result.stderr)
            raise Exception(f"Error creating environment '{env_name}'.")

        # env_details = json.loads(result.stdout)

        check = self._check()
        if check is not None:
            return check
        else:
            raise Exception("Something went wrong. This is a bug.")


class CondaBuildPackageConfig(OneOffConfig):

    package_name: str = Field(description="The name of the Python package.")
    version: Union[str] = Field(description="The version of the package.", default=None)


class CondaBuildPackageDetails(StateDetails):

    base_dir: str = Field(description="The base directory.")
    build_dir: str = Field(description="The build directory.")
    meta_file: str = Field(description="The path to the package meta file.")
    package: PkgSpec = Field(description="Package metadata.")
    build_artifact: str = Field(description="Path to the package build artifact.")
    run_details: RunDetails = Field(description="The details of the build process run.")

class CondaPackageBuilt(OneOff[CondaBuildPackageConfig, CondaBuildPackageDetails]):

    def _retrieve_dependencies(self) -> Mapping[str, Union[State, Type[State]]]:

        deps = {
            "conda-build-env": MambaEnvironment.create_from_config(env_name="conda-build-env")
        }
        return deps

    def _resolve(self) -> Union[Mapping[str, Any], StateDetails]:
        pass

        build_env_details: MambaEnvironmentDetails = self.get_dependency_details("conda-build-env")  # type: ignore

        env_name = build_env_details.env_name
        prefix = build_env_details.mamba_prefix
        conda_bin = os.path.join(prefix, env_name, "bin", "conda")

        # tempdir = tempfile.TemporaryDirectory()
        # base_dir = tempdir.name
        base_dir = os.path.join(
            self.state_dir.as_posix(),
            "build",
            package.pkg_name,
            package.pkg_version,
            f"python-{python_version}",
        )

        build_dir = Path(base_dir) / "build"
        if build_dir.is_dir():
            shutil.rmtree(build_dir)
        build_dir.mkdir(parents=True, exist_ok=False)

        meta_file = Path(base_dir) / "meta.yaml"
        recipe = package.create_conda_spec()
        with open(meta_file, "wt") as f:
            f.write(recipe)

        channels = [
            item
            for tokens in (("--channel", channel) for channel in package.pkg_channels)
            for item in tokens
        ]

        args = ["mambabuild", "--py", python_version]
        args.extend(channels)
        args.extend(["--output-folder", build_dir.as_posix(), base_dir])

        result = execute(
            conda_bin,
            *args,
            stdout_callback=default_stdout_print,
            stderr_callback=default_stderr_print,
        )

        artifact = os.path.join(
            build_dir,
            "noarch",
            f"{package.pkg_name}-{package.pkg_version}-py_0.tar.bz2",
        )
        if not Path(artifact).is_file():
            raise Exception(f"Invalid artifact path: {artifact}")

        result = CondaBuildPackageDetails(
            cmd=conda_bin,
            args=args[1:],
            stdout=result.stdout,
            stderr=result.stderr,
            exit_code=result.exit_code,
            base_dir=base_dir,
            build_dir=build_dir.as_posix(),
            meta_file=meta_file.as_posix(),
            package=package,
            build_artifact=artifact,
        )
        return result
