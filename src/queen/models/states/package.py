import os
from typing import Union, Mapping, Type, Any, Dict, Iterable

import httpx
import orjson
from pydantic import Field

from queen.models import PkgSpec, DEFAULT_HOST_DEPENDENCIES
from queen.models.states import OneOffConfig, StateDetails, OneOff, State
from queen.models.states.mamba import MambaEnvironment, MambaEnvironmentDetails
from queen.utils.execute import execute
from queen.utils.python_packages import get_pkg_metadata_from_pypi


class PackageConfig(OneOffConfig):

    pkg: str = Field(description="The name of (or path to) the Python package.")
    version: Union[str, None] = Field(description="The version of the package.", default=None)

class PackageMetadata(StateDetails):

    package_name: str = Field(description="The name of the Python package.")
    version: str = Field(description="The version of the package.")
    metadata: Mapping[str, Any] = Field(description="The metadata of the package.")


class CreatePackageMetadata(OneOff[PackageConfig, PackageMetadata]):

    def _retrieve_dependencies(self) -> Mapping[str, Union["State", Type["State"]]]:

        deps = {}

        pkg = self.config.pkg
        path = os.path.realpath(os.path.expanduser(pkg))

        if os.path.isdir(path):
            deps["conda-build-env"] = MambaEnvironment.create_from_config(env_name="conda-build-env")

        return deps

    def get_pkg_metadata_from_project_folder(
        self, project_path: str, force_version: Union[str, None] = None
    ) -> Mapping[str, Any]:

        build_env_details: MambaEnvironmentDetails = self.get_dependency_details("conda-build-env")  # type: ignore
        env_name = build_env_details.env_name
        prefix = build_env_details.mamba_prefix

        project_path = os.path.abspath(
            os.path.realpath(os.path.expanduser(project_path))
        )
        if project_path.endswith(os.path.sep):
            project_path = project_path[0:-1]

        pip_cmd = os.path.join(prefix, env_name, "bin", "pip")
        args = ["install", "--quiet", "--dry-run", "--report", "-", project_path]

        result = execute(pip_cmd, *args)
        pkg_metadata = orjson.loads(result.stdout)

        install_list = pkg_metadata["install"]
        result = None
        for install_item in install_list:
            # TODO: windows?
            if (
                install_item.get("download_info", {}).get("url", "")
                == f"file://{project_path}"
            ):
                result = install_item["metadata"]
        if not result:
            raise Exception(f"Could not parse package metadata for: {project_path}")

        folder_name = os.path.basename(project_path)
        if folder_name != result["name"]:
            if folder_name == result["name"].replace("-", "_"):
                result["name"] = folder_name
            elif folder_name.startswith("kiara_plugin.") and result["name"].startswith(
                "kiara-plugin"
            ):
                result["name"] = result["name"].replace("-", "_", 1)

        assert "releases" not in result.keys()

        if force_version:
            result["version"] = force_version
        version = result["version"]
        result["releases"] = {}
        result["releases"][version] = [
            {"url": f"file://{project_path}", "packagetype": "project_folder"}
        ]
        return result

    def _resolve(self) -> PackageMetadata:

        pkg = self.config.pkg
        version = self.config.version
        path = os.path.realpath(os.path.expanduser(pkg))

        force_version = False

        if os.path.isdir(path):
            if version:
                if not force_version:
                    raise Exception(
                        "Specified project is a local folder, using 'version' with this does not make sense."
                    )
            pkg_metadata = self.get_pkg_metadata_from_project_folder(
                path, force_version=version
            )
            pkg_name = pkg_metadata["name"]
            pkg_version = pkg_metadata["version"]

        else:
            pkg_name = pkg
            pkg_metadata = get_pkg_metadata_from_pypi(
                pkg_name=pkg, version=version
            )
            pkg_version = "xxx"

        result = PackageMetadata(package_name=pkg_name, version=pkg_version, metadata=pkg_metadata)
        return result


class BuildPackageSpecConfig(PackageConfig):

    patch_data: Mapping[str, Any] = Field(description="The patch data to apply to the package metadata.", default_factory=dict)

class BuildPkgSpecDetails(StateDetails):

    package: PkgSpec = Field(description="Package metadata.")


class BuildPackageSpec(OneOff[BuildPackageSpecConfig, BuildPkgSpecDetails]):

    def _retrieve_dependencies(self) -> Mapping[str, Union[State, Type[State]]]:

        deps = {
            "package_metadata": CreatePackageMetadata.create_from_config(pkg=self.config.pkg, version=self.config.version)
        }
        return deps

    def _resolve(self) -> BuildPkgSpecDetails:

        req_repl_dict = None
        patch_data = self.config.patch_data

        if patch_data:
            req_repl_dict = patch_data.get("requirements", None)

        pkg_metadata_details: PackageMetadata = self.get_dependency_details("package_metadata")  # type: ignore
        pkg_metadata = pkg_metadata_details.metadata

        requirements = self.extract_reqs_from_metadata(pkg_metadata=pkg_metadata)
        req_list = []
        for k, v in requirements.items():
            if req_repl_dict and k in req_repl_dict.keys():
                repl = req_repl_dict[k]
                if repl:
                    req_list.append(req_repl_dict[k])
            else:
                if not v.get("version"):
                    pkg_str = k
                else:
                    pkg_str = f"{k} {v['version']}"
                req_list.append(pkg_str)

        pkg_name = pkg_metadata["name"]
        version = pkg_metadata["version"]

        # all_data = self.get_all_pkg_data_from_pypi(pkg_name=pkg_name)

        releases = pkg_metadata["releases"]
        if pkg_metadata["version"] not in releases.keys():
            raise Exception(
                f"Could not find release '{version}' data for package '{pkg_name}'."
            )

        version_data = releases[pkg_metadata["version"]]

        pkg_hash = None
        pkg_url = None
        for v in version_data:
            if v["packagetype"] == "project_folder":
                pkg_hash = None
                pkg_url = v["url"]
                break

        if pkg_hash is None:
            for v in version_data:
                if v["packagetype"] == "sdist":
                    pkg_hash = v["digests"]["sha256"]
                    pkg_url = v["url"]
                    break

        if pkg_hash is None:
            for v in version_data:
                if v["packagetype"] == "bdist_wheel":
                    # TODO: make sure it's a universal wheel
                    pkg_hash = v["digests"]["sha256"]
                    pkg_url = v["url"]

        if pkg_url is None:
            raise Exception(f"Could not find hash for package: {pkg_name}.")

        pkg_requirements = req_list
        if patch_data and "channels" in patch_data.keys():
            pkg_channels = patch_data["channels"]
        else:
            pkg_channels = ["conda-forge"]
        recipe_maintainers = ["frkl"]

        if patch_data and "host_requirements" in patch_data.keys():
            host_requirements = patch_data["host_requirements"]
        else:
            host_requirements = DEFAULT_HOST_DEPENDENCIES()

        if patch_data and "test" in patch_data.keys():
            test_spec = patch_data["test"]
        else:
            test_spec = {}

        home_page = pkg_metadata.get("home_page", None)
        if not home_page:
            for url in pkg_metadata.get("project_url", []):
                if url.startswith("homepage, "):
                    home_page = url[10:]
                    break

        spec_data = {
            "pkg_name": pkg_name,
            "pkg_version": pkg_metadata["version"],
            "pkg_hash": pkg_hash,
            "pkg_url": pkg_url,
            "host_requirements": host_requirements,
            "pkg_requirements": pkg_requirements,
            "pkg_channels": pkg_channels,
            "metadata": {
                "home": home_page,
                "license": pkg_metadata.get("license"),
                "summary": pkg_metadata.get("summary"),
                "recipe_maintainers": recipe_maintainers,
            },
            "test": test_spec,
        }

        pkg_spec = PkgSpec(**spec_data)  # type: ignore
        result = BuildPkgSpecDetails(package=pkg_spec)
        return result

    def extract_reqs_from_metadata(
        self, pkg_metadata: Mapping[str, Any], extras: Union[None, Iterable[str]] = None
    ) -> Dict[str, Dict[str, Any]]:

        reqs = pkg_metadata.get("requires_dist", None)

        if not reqs:
            return {}

        filtered_reqs = {}
        extras_reqs = {}
        for r in reqs:
            tokens = r.split(";")
            if len(tokens) == 1:
                pkg_tokens = tokens[0].strip().split(" ")
                if len(pkg_tokens) == 1:
                    pkg = pkg_tokens[0]
                    ver = None
                elif len(pkg_tokens) == 2:
                    pkg = pkg_tokens[0]
                    ver = pkg_tokens[1][1:-1]
                else:
                    raise Exception(f"Can't parse version for pkg: {tokens[0]}")
                cond = None
            elif len(tokens) == 2:
                if "extra" in tokens[1]:
                    extra_start = tokens[1].index("extra == ")
                    substr = tokens[1][extra_start + 10 :]
                    extra_stop = substr.index("'")
                    extra_name = substr[0:extra_stop]
                    # TODO: multiple extras possible?
                    if not extras or extra_name not in extras:
                        continue
                cond = tokens[1].strip()
                pkg_tokens = tokens[0].strip().split(" ")
                if len(pkg_tokens) == 1:
                    pkg = pkg_tokens[0]
                    ver = None
                elif len(pkg_tokens) == 2:
                    pkg = pkg_tokens[0]
                    ver = pkg_tokens[1][1:-1]
                else:
                    raise Exception(f"Can't parse version for pkg: {tokens[0]}")
                if ver:
                    ver = ver[1:-1]

            else:
                raise Exception(f"Can't parse requirement: {r}")

            if pkg in filtered_reqs.keys():
                raise Exception(f"Duplicate req: {pkg}")

            if "[" in pkg:
                extras_pkg = pkg[0 : pkg.index("[")]
                extras_substr = pkg[pkg.index("[") + 1 :]
                extras_str = extras_substr[: extras_substr.index("]")]
                extras_list = extras_str.split(",")
                extras_reqs[extras_pkg] = extras_list
                assert extras_pkg not in filtered_reqs.keys()
                filtered_reqs[extras_pkg] = {"version": ver, "condition": cond}
            else:
                assert pkg not in filtered_reqs.keys()
                filtered_reqs[pkg] = {"version": ver, "condition": cond}

        for extra_pkg, extras in extras_reqs.items():
            # version = filtered_reqs[extra_pkg]["version"]
            # TODO: figure out the right version if there's a condition
            version = None
            req_metadata = get_pkg_metadata_from_pypi(
                pkg_name=extra_pkg, version=version
            )
            new_reqs = self.extract_reqs_from_metadata(req_metadata, extras=extras)
            for k, v in new_reqs.items():
                if k in filtered_reqs.keys():
                    continue
                filtered_reqs[k] = v

        fixed = {}
        for k in sorted(filtered_reqs.keys()):
            if k.startswith("kiara-plugin"):
                fixed[k.replace("-", "_")] = filtered_reqs[k]
            else:
                fixed[k] = filtered_reqs[k]

        return fixed
