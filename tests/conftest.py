#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Copyright (c) 2021, University of Luxembourg / DHARPA project
#  Copyright (c) 2021, Markus Binsteiner
#
#  Mozilla Public License, version 2.0 (see LICENSE or https://www.mozilla.org/en-US/MPL/2.0/)

"""
    Dummy conftest.py for kiara.

    If you don't know what this is for, just leave it empty.
    Read more about conftest.py under:
    https://pytest.org/latest/plugins.html
"""
from typing import Union, Any, Mapping, Type

import pytest

import os
import tempfile
import uuid
from pathlib import Path

from queen.mgmt import StateContext
from queen.models.states import State, StateConfig, StateDetails


class TestConfigA(StateConfig):
    prop_1: str
    prop_2: str

class TestDetailsA(StateDetails):
    prop_1: str
    prop_2: str

class TestStateA(State[TestConfigA, TestDetailsA]):

    def _check(self) -> Union[None, TestDetailsA]:
        return None

    def _resolve(self) -> Union[Mapping[str, Any], TestDetailsA]:

        return TestDetailsA(prop_1="value_1", prop_2="value_2")

class TestConfigB(StateConfig):
    prop_1: str
    prop_2: str
    raise_error: bool = False

class TestDetailsB(StateDetails):
    prop_1: str
    prop_2: str

class TestStateB(State[TestConfigB, TestDetailsB]):

    def _retrieve_dependencies(self) -> Mapping[str, Union["State", Type["State"]]]:
        return {"test_state_a": TestStateA}

    def _check(self) -> Union[None, TestDetailsB]:
        return None

    def _resolve(self) -> Union[Mapping[str, Any], TestDetailsB]:

        dep_value_1: TestDetailsA = self.get_dependency_details("test_state_a")  # noqa

        if self.config.raise_error:
            if not self.config.prop_1 == dep_value_1.prop_1:
                raise Exception(f"Values don't match 1: {self.config.prop_1} != {dep_value_1.prop_1}")

            if not self.config.prop_2 == dep_value_1.prop_2:
                raise Exception(f"Values don't match 2: {self.config.prop_1} != {dep_value_1.prop_2}")

        return TestDetailsB(prop_1=dep_value_1.prop_1, prop_2=dep_value_1.prop_2)

@pytest.fixture
def state_context() -> StateContext:

    context = StateContext()

    fr = "629143448"

    context.add_state(state=TestStateA.create_from_config(prop_1="config_1", prop_2="config_2"), state_alias="state_a")
    context.add_state(state=TestStateB.create_from_config(prop_1="config_1", prop_2="config_2"), state_alias="state_b")

    return context
